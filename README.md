# JUIT API Gateway

O API Gateway usado pela JUIT é o KrakenD.


# Estrutura atual

O KrakenD foi instalado diretamente no SO (não depende de um container para ser executado) e é executado como um serviço do próprio Linux.

A distribuição Linux utilizada é a CentOS Stream 8.

Informações do SO:
```sh
uname -a
``` 
*Linux juit-api-gateway-sa-east1-b-prd 4.18.0-358.el8.x86_64 #1 SMP Mon Jan 10 13:11:20 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux*

Informações do SO:
```sh
hostnamectl
``` 
   *Static hostname: juit-api-gateway-sa-east1-b-prd
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 710bc49eca98c9d1b6331291fa364dc4
           Boot ID: 63817515c8fc4ab3a6dc77fd8e75d1d5
    Virtualization: kvm
  Operating System: CentOS Stream 8
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-358.el8.x86_64
      Architecture: x86-64*

### Acessos
 O recomendado é que o acesso e todas as alterações feitas no ambiente, sejam feitas pelo usuário **juit-ops**.

Acesso ao servidor:
```powershell
gcloud beta compute ssh juit-ops@juit-api-gateway-sa-east1-b-prd --project=site-216302 --zone=southamerica-east1-b
``` 

## Configuração

A configuração do KrakenD pode ser feita diretamente no arquivo de configuração krakend.json ou através de uma ferramenta disponibilizada pela própria KrakenD, o KrakenDesigner: [https://designer.krakend.io](https://designer.krakend.io)
O arquivo de configuração do KrakenD, fica no caminho: **/etc/krakend/krakend.json**

### Certificados
Para que o KrakenD possa fornecer acesso via HTTPS, é preciso que os arquivos dos certificados estejam no mesmo servidor. Para a instalação da Juit, seguem os detalhes dos arquivos de certificado:

 - Chave Pública - /etc/juit/cert.pem
 - Chave Privada - /etc/juit/privkey.pem

## Gestão do serviço / daemon do KrakenD

Verificando o status do serviço do KrakenD:
```sh
sudo service krakend status
``` 

Iniciando o serviço do KrakenD:
```sh
sudo service krakend start
``` 
Parando o serviço do KrakenD:
```sh
sudo service krakend start
``` 

Quando o arquivo de configuração for modificado, será preciso fazer o restart do KrakenD:
```sh
sudo service krakend restart && sudo service krakend status
``` 

## Logs
O KrakenD foi configurado para registrar os logs no log central do Linux, que fica disponível em:   **/var/log/messages**

Para acompanhar os logs em tempo real, basta usar a seguinte linha de comando:
```sh
sudo tail -f /var/log/messages | grep krakend
``` 

# Referências
A documentação oficial do KrakenD, fica disponível em: [https://www.krakend.io/docs/overview/introduction/](https://www.krakend.io/docs/overview/introduction/)