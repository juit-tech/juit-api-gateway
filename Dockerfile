FROM devopsfaith/krakend

COPY ./config/settings/krakend.json /etc/krakend/krakend.json
COPY ./config/certificates/cert.pem /etc/juit/cert.pem
COPY ./config/certificates/privkey.pem /etc/juit/privkey.pem